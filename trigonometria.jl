        #Mac0110 - MiniEP7
        #Luiz Carlos Costa da Silva - 11261851

function sin(x)  #Função que calcula o sin de x 
    v = []       #Vetor para somar todos os valores 
    push!(v,x)   #Adicionar x ao vetor v 
    sinal = -1
    pot = 3 
    den = 3  
        for i in 1:10
           formula = sinal*((x^pot)/(factorial(big(den))))
           push!(v,formula)       #adicionar a formula ao vetor v 
           sinal = sinal*-1 
           pot =  pot+2
           den =  den+2  
        end
    return sum(v)  #Somar todos os elementos do vetor 
end 
    
function cos(x)  # Função que calcula cos x 
    v = []   # Vetor para somar todos os valores 
    push!(v,1)  # Adicionar 1 ao vetor v
    sinal = -1
    pot = 2 
    den = 2  
        for i in 1:10
            formula = sinal*((x^pot)/(factorial(big(den))))
            push!(v,formula)     # Adicionar a formula ao vetor v
            sinal = sinal*-1 
            pot =  pot+2
            den =  den+2  
        end
    return sum(v)  
end 

function tan(x)
  v = []  
     for n in 1:10
    formula = ((2^2n) * ((2^2n)-1)*bernoulli(n)*(x^(2n-1)))/(factorial(big(2n))) 
    push!(v,formula)
    #@show v
     end
     return sum(v)
end

function bernoulli(n)
    n *= 2
    A = Vector{Rational{BigInt}}(undef, n + 1)
    for m = 0 : n
        A[m + 1] = 1 // (m + 1)
        for j = m : -1 : 1
            A[j] = j * (A[j] - A[j + 1])
        end
    end
    return abs(A[1])
end

function check_sin(value, x)
    erro = 0.001 
    igual = abs(value - x)

    if igual <= erro 
        return true
    else 
        return false
    end
end

function check_cos(value, x)
    erro = 0.001 
    igual = abs(value - x)
    if igual <= erro 
        return true
    else 
        return false
    end
end

function check_tan(value, x)
    erro = 0.001 
    igual = abs(value - x)
    if igual <= erro 
        return true
    else 
        return false
    end
end

function taylor_sin(x)  #Função que calcula o sin de x 
    v = []       #Vetor para somar todos os valores 
    push!(v,x)   #Adicionar x ao vetor v 
    sinal = -1
    pot = 3 
    den = 3  
        for i in 1:10
           formula = sinal*((x^pot)/(factorial(big(den))))
           push!(v,formula)       #adicionar a formula ao vetor v 
           sinal = sinal*-1 
           pot =  pot+2
           den =  den+2  
        end
    return sum(v)  #Somar todos os elementos do vetor 
end 
    
function taylor_cos(x)  # Função que calcula cos x 
    v = []   # Vetor para somar todos os valores 
    push!(v,1)  # Adicionar 1 ao vetor v
    sinal = -1
    pot = 2 
    den = 2  
        for i in 1:10
            formula = sinal*((x^pot)/(factorial(big(den))))
            push!(v,formula)     # Adicionar a formula ao vetor v
            sinal = sinal*-1 
            pot =  pot+2
            den =  den+2  
        end
    return sum(v)  
end 

function taylor_tan(x)
  v = []  
     for n in 1:10
    formula = ((2^2n) * ((2^2n)-1)*bernoulli(n)*(x^(2n-1)))/(factorial(big(2n))) 
    push!(v,formula)
    @show v
     end
     return sum(v)
end

using Test
function test()
@test check_sin(0.5,sin(pi/6))    == true
@test check_sin(1.0,sin(pi/2))    == true
@test check_sin(0,  sin(pi))      == true 
@test check_cos(0.5,cos(pi/3))    == true 
@test check_cos(-1, cos(pi))      == true 
@test check_cos(0, cos(pi/2))     == true
@test check_tan(sqrt(3),tan(pi/3)) == true 
@test check_tan((sqrt(3)/3),tan(pi/6))    == true 
end
